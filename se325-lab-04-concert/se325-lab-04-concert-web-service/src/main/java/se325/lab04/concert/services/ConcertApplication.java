package se325.lab04.concert.services;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("/services")
public class ConcertApplication extends Application {
	
	private Set<Object> singletons = new HashSet<>();
	private Set<Class<?>> classes = new HashSet<>();
	
	public ConcertApplication() {
		singletons.add(PersistenceManager.instance());
		classes.add(ConcertResource.class);
		classes.add(SerializationMessageBodyReaderAndWriter.class);
	}
	
	@Override
    public Set<Object> getSingletons() {
        return singletons;
    }
	
	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}
}
