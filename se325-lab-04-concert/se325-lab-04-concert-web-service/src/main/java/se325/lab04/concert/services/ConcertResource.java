package se325.lab04.concert.services;

import static se325.lab04.concert.services.SerializationMessageBodyReaderAndWriter.APPLICATION_JAVA_SERIALIZED_OBJECT;

import java.net.URI;

import javax.persistence.EntityManager;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import se325.lab04.concert.domain.Concert;

// Based of lab 3, just converted to use PersistanceManager

@Path("/concerts")
public class ConcertResource {

	private static Logger LOGGER = LoggerFactory.getLogger(ConcertResource.class);

	@GET
	@Path("{id}")
	@Produces({ APPLICATION_JAVA_SERIALIZED_OBJECT, MediaType.APPLICATION_JSON })
	public Response retrieveConcert(@PathParam("id") long id) {
		EntityManager em = createEM();
		try {
			em.getTransaction().begin();
			Concert foundConcert = em.<Concert>find(Concert.class, id);
			em.getTransaction().commit();

			if (foundConcert == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
			LOGGER.info("Retrived concert # " + id);
			return Response.ok(foundConcert).build();

		} finally {
			em.close(); // Will get called even if we return
		}
	}

	@POST
	@Consumes({ APPLICATION_JAVA_SERIALIZED_OBJECT, MediaType.APPLICATION_JSON })
	public Response createConcert(Concert newConcert) {
		EntityManager em = createEM();
		try {

			em.getTransaction().begin();
			em.persist(newConcert);
			em.getTransaction().commit();

			LOGGER.info("Created concert with id: " + newConcert.getId());
			return Response.created(URI.create("/concerts/" + newConcert.getId())).build();

		} finally {
			em.close();
		}
	}

	@PUT
	@Consumes({ APPLICATION_JAVA_SERIALIZED_OBJECT, MediaType.APPLICATION_JSON })
	public Response replaceConcert(Concert concert) {
		EntityManager em = createEM();
		try {
			em.getTransaction().begin();
			Concert foundConcert = em.<Concert>merge(concert);
			em.getTransaction().commit();
			
			if (foundConcert == null) {
				return Response.status(Status.NOT_FOUND).build();
			}
			LOGGER.info("Updated concert with id: " + concert.getId());
			return Response.noContent().build();

		} finally {
			em.close();
		}
	}

	@DELETE
	@Path("{id}")
	public Response deleteConcert(@PathParam("id") long id) {
		EntityManager em = createEM();
		try {
			em.getTransaction().begin();
			Concert foundConcert = em.<Concert>find(Concert.class, id);
			if(foundConcert != null) {
				em.remove(foundConcert);
			}
			em.getTransaction().commit();
			
			if (foundConcert == null) {
				return Response.status(Status.NOT_FOUND).build();
			}

			LOGGER.info("Deleted concert with id: " + foundConcert.getId());
			return Response.noContent().build();

		} finally {
			em.close();
		}
	}

	@DELETE
	public Response deleteAllConcerts() {
		EntityManager em = createEM();
		try {
			em.getTransaction().begin();
			int deleted = em.createQuery("DELETE FROM Concert").executeUpdate();
			em.getTransaction().commit();

			LOGGER.info("Deleted all (" + deleted + ") concerts");
			return Response.noContent().build();
		} finally {
			em.close();
		}
	}

	private EntityManager createEM() {
		return PersistenceManager.instance().createEntityManager();
	}
}
